(a) A short outline of iterations made.

First iteration was just getting all the code to compile

Second iteration was getting the tests to run

third iteration was working on getting build paths into different areas

fourth iteration was splitting the course tree

fifth iteration was moving the jar file

(b) A reflection of observed benefits and liabilities of using the rhythm and  small steps in refactoring the build environment.

Seemed fine. Honestly ant scripts seem like one of those things where copy pasting boiler code works really well. I am unsure if it is worth the time
to build up an ant script like that every time, but what do I know.

Benefit: I always knew what worked

Liabilities: Waste of time maybe? Lots of issues setting up file systems if I didn't know what was wrong the first time. Lots of small mistakes form because of messed
up file systems and redoing the iteration to fix them.
